#!/usr/bin/env node
const chalk = require('chalk');
var amqp = require('amqplib/callback_api');

var qName = 'znpAPI';

async function publish(ex, routingKey, data) {
    return new Promise(
        (resolve, reject) => {
            amqp.connect('amqp://pixelped:Pixelti@1700@191.252.113.160', function (err, conn) {
                conn.createChannel(function (err, ch) {
                    ch.assertQueue('', { exclusive: true }, function (err, q) {
                        var corr = generateUuid();
                        console.log(chalk.cyan(' [x] Requesting - ', JSON.stringify(data)));

                        ch.consume(q.queue, function (msg) {
                            if (msg.properties.correlationId == corr) {
                                console.log(chalk.blue(' [.] Got - ', msg.content.toString()));
                                resolve(msg.content);
                                setTimeout(function () { conn.close(); }, 500);
                            }else reject("sem resposta");
                        }, { noAck: true });

                        ch.assertExchange(ex, 'direct', { durable: false });
                        ch.publish(ex, routingKey, new Buffer(JSON.stringify(data)), { correlationId: corr, replyTo: q.queue });
                        setTimeout(function () { conn.close(); }, 5000);
                    });
                });
            });
        }
    );
}

function generateUuid() {
    return Math.random().toString() +
           Math.random().toString() +
           Math.random().toString();
}

module.exports = {
    publish: publish
}
