var express = require('express');
var router = express.Router();
var pub = require('../routes/publisher');
const CircularJSON = require('circular-json');

var routingKey = 'cmd';

/*************** ZSHEPHERD FUNCTIONS ***************/
//START
router.get('/start', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "start",
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//STOP
router.get('/stop', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "stop",
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//RESET
//{id: 0 - 1}
router.get('/reset/:id', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "reset",
        "data": {
          "mode": req.params.id
        }
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//PERMIT JOIN
//{time: 0 - 255}
router.get('/join/:time', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "join",
        "data": {
          "joinTime": Number(req.params.time)
        }
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//INFO
router.get('/info', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "info",
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//MOUNT
//{ zApp: ZiveObject }
router.post('/mount', function (req, res, next) {
  res.send('respond with a resource');
});

//LIST
//[{ ieeeAddr: string }]
router.get('/list', function (req, res, next) {
  (async () => {
    try {
      var data = {
        "type": "list"
      };
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//FIND
//{ addr: string, epId: number }
router.post('/find', function (req, res, next) {
  data = {
    "type": "find",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": Number(req.body.epId)
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(JSON.parse(CircularJSON.stringify(result)));
    } catch (err) {
      res.send(err.message);
    }
  })();
});

//LINK QUALITY INDEX
//{ ieeeAddr: string }
router.post('/lqi', function (req, res, next) {
  data = {
    "type": "lqi",
    "data": {
      "ieeeAddr": req.body.ieeeAddr
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err.meesage);
    }
  })();
});

//REMOVE
//{ ieeeAddr: string, cfg: [{ reJoin: boolean, rmChildren: boolean }] }
router.post('/remove', function (req, res, next) {
  data = {
    "type": "remove",
    "data": {
      "ieeeAddr": req.body.ieeeAddr
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});

//ACCEPT DEVICE INCOMING
//{ devInfo: devInfo Object }
router.post('/acceptDevIncoming', function (req, res, next) {
  data = {
    "type": "acceptDevIncoming",
    "data": {
      "devInfo": req.body
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});
/*************** *************** ***************/



/************** ENDPOINT FUNCTIONS **************/
//GETSIMPLEDESC
//{ieeeAddr: string, epId: number}
router.post('/getSimpleDesc', function (req, res, next) {
  data = {
    "type": "getSimpleDesc",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": Number(req.body.epId)
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});

//FOUNDATION
//{ieeeAddr: string, epId: number, cId: string, cmd: string, zclData: {Object} }
router.post('/foundation', function (req, res, next) {
  data = {
    "type": "foundationToDevice",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": req.body.epId,
      "cId": req.body.cId,
      "cmd": req.body.cmd,
      "zclData": req.body.zclData
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});

//FUNCTIONAL
//{ieeeAddr: string, epId: number, cId: string, cmd: string, zclData: {Object} }
router.post('/functional', function (req, res, next) {
  data = {
    "type": "functionalToDevice",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": req.body.epId,
      "cId": req.body.cId,
      "cmd": req.body.cmd,
      "zclData": req.body.zclData
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});

//READ
//{ieeeAddr: string, epId: number, cId: string, attrId: string }
router.post('/read', function (req, res, next) {
  data = {
    "type": "read",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": req.body.epId,
      "cId": req.body.cId,
      "attrId": req.body.attrId,
    }
  };
  (async () => {
    let result = await pub.publish('gtw001', 'cmd', data);
    res.send(result);
  })();
});

//WRITE
//{ieeeAddr: string, epId: number, cId: string, attrId: string, data: {Object} }
router.post('/write', function (req, res, next) {
  data = {
    "type": "write",
    "data": {
      "devIeeeAddr": req.body.ieeeAddr,
      "devEpId": req.body.epId,
      "cId": req.body.cId,
      "attrId": req.body.attrId,
      "data": req.body.data
    }
  };
  (async () => {
    let result = await pub.publish('gtw001', 'cmd', data);
    res.send(result);
  })();
});

//BIND
//{cId: string, dev1: { ieeeAddr: string, epId: number }, dev2: { ieeeAddr: string, epId: number } }
router.post('/bind', function (req, res, next) {
  data = {
    "type": "bind",
    "data": {
      "cId": req.body.cId,
      "dev1": {
        "ieeeAddr": req.body.dev1.ieeeAddr,
        "epId": req.body.dev1.epId
      },
      "dev2": {
        "ieeeAddr": req.body.dev2.ieeeAddr,
        "epId": req.body.dev2.epId
      }
    }
  }
    (async () => {
      try {
        console.log(chalk.orange(data));
        let result = await pub.publish('gtw001', 'cmd', data);
        res.send(result);
      } catch (err) {
        res.send(err);
      }
    })();
});

//UNBIND
//{cId: string, dev1: { ieeeAddr: string, epId: number }, dev2: { ieeeAddr: string, epId: number } }
router.post('/unbind', function (req, res, next) {
  data = {
    "type": "unbind",
    "data": {
      "cId": req.body.cId,
      "dev1": {
        "ieeeAddr": req.body.dev1.ieeeAddr,
        "epId": req.body.dev1.epId
      },
      "dev2": {
        "ieeeAddr": req.body.dev2.ieeeAddr,
        "epId": req.body.dev2.epId
      }
    }
  }
    (async () => {
      try {
        let result = await pub.publish('gtw001', 'cmd', data);
        res.send(result);
      } catch (err) {
        res.send(err);
      }
    })();
});

//DUMP
//{ieeeAddr: string, epId: number}
router.post('/dump', function (req, res, next) {
  data = {
    "type": "dump",
    "data": {
      "ieeeAddr": req.body.ieeeAddr,
      "epId": req.body.epId
    }
  };
  (async () => {
    try {
      let result = await pub.publish('gtw001', 'cmd', data);
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  })();
});

module.exports = router;
