#!/usr/bin/env node
const chalk = require('chalk');
var amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);

if (args.length == 0) {
    console.log("Usage: publisher.js exchange routingKey cmd");
    process.exit(1);
}

amqp.connect('amqp://pixelped:Pixelti@1700@191.252.113.160', function (err, conn) {
    if (!err) {
        conn.createChannel(function (err, ch) {
            console.log(chalk.cyan("Enviei via Cloud"));
            var ex = args[0];
            var routingKey = args[1];
            var msg = args[2];
            var qName = 'znpAPI';

            ch.assertExchange(ex, 'direct', { durable: false });
            ch.publish(ex, routingKey, new Buffer(msg));
            console.log(chalk.magenta(" [x] Sent %s: '%s'", routingKey, msg));
        });
        setTimeout(function () { conn.close(); process.exit(0) }, 500);
    } else {
        amqp.connect('amqp://pixelped:Pixelti@1700@192.168.0.223', function (err, conn) {
            if (!err) {
                conn.createChannel(function (err, ch) {
                    console.log(chalk.cyan("Enviei via Local"));
                    var ex = args[0];
                    var routingKey = args[1];
                    var msg = args[2];
                    var qName = 'znpAPI';

                    ch.assertExchange(ex, 'direct', { durable: false });
                    ch.publish(ex, routingKey, new Buffer(msg));
                    console.log(chalk.inverse.magenta(" [x] Sent %s: '%s'", routingKey, msg));
                });
                setTimeout(function () { conn.close(); process.exit(0) }, 500);
            } else {
                console.log(chalk.red(" [x] DEU RUIM NO BROKER [x] "));
            }
        });
    }
});
