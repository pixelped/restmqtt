#!/usr/bin/env node
var amqp = require('amqplib/callback_api');
const chalk = require('chalk');

amqp.connect('amqp://pixelped:Pixelti@1700@191.252.113.160', function (err, conn) {
    conn.createChannel(function (err, ch) {
        var ex = 'gtw001';
        var qName = 'znpAPI';

        ch.assertExchange(ex, 'direct', { durable: false });
        ch.assertQueue('', { exclusive: true }, function (err, q) {
            console.log(chalk.bold.magenta(' [x] Awaiting RPC requests'));

            ch.bindQueue(q.queue, ex, 'report');


            ch.consume(q.queue, function (msg) {
                switch (msg.content.type) {
                    case 'attReport':
                        console.log(chalk.green(" [.] ", msg.fields.routingKey, ": ", msg.content.toString()));
                        break;
                    case 'devChange':
                        console.log(chalk.red(" [.] ", msg.fields.routingKey, ": ", msg.content.toString()));
                        break;
                    case 'devIncoming':
                        console.log(chalk.yellow(" [.] ", msg.fields.routingKey, ": ", msg.content.toString()));
                        break;
                    case 'devLeaving':
                        console.log(chalk.yellow(" [.] ", msg.fields.routingKey, ": ", msg.content.toString()));
                        break;
                    default:
                        console.log(chalk.yellow(" [.] ", msg.fields.routingKey, ": ", msg.content.toString()));
                        break;
                }

            }, { noAck: true });
        });
    });
});
